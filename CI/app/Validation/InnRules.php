<?php

namespace App\Validation;
class InnRules
{

	public function inn1(string $inn = null):bool
	{
		$count = strlen( trim( $inn ) );
		return $count == 12;
	}
	
	public function inn2(string $inn = null):bool
	{
		if (preg_match('#([\d]{12})#', $inn, $m)) {
			$inn = $m[0];
			$code11 = (($inn[0] * 7 + $inn[1] * 2 + $inn[2] * 4 + $inn[3] *10 +
						$inn[4] * 3 + $inn[5] * 5 + $inn[6] * 9 + $inn[7] * 4 +
						$inn[8] * 6 + $inn[9] * 8) % 11 ) % 10;
			$code12 = (($inn[0] * 3 + $inn[1] * 7 + $inn[2] * 2 + $inn[3] * 4 +
						$inn[4] *10 + $inn[5] * 3 + $inn[6] * 5 + $inn[7] * 9 +
						$inn[8] * 4 + $inn[9] * 6 + $inn[10]* 8) % 11 ) % 10;
						
			return ($code11 == $inn[10] && $code12 == $inn[11]);
		}
		return false;
	}

	public function innOnline(string $inn = null):bool
	{
		$db = \Config\Database::connect();
		$row = $db->table('inn')
				  ->select('*')
				  ->where('inn', $inn)
				  ->limit(1)
				  ->get();
		if ( $row->getRow() ) {
			$_row = $row->getRow();
			$now = time(); // or your date as well
			$your_date = strtotime($_row->date_added);
			$datediff = $now - $your_date;


			if ( round($datediff / (60 * 60 * 24)) > 1 ) {
				$db->table('inn')->where('inn_id', $_row->inn_id)->delete();
				return $this->_curl($inn);
			}
			return (bool)$_row->result;
		}
		return $this->_curl($inn);

	}

	public function _curl( $inn ):bool
	{

		$date = new \DateTime("now");;
		$dateStr = $date->format("Y-m-d");
		$url = "https://statusnpd.nalog.ru/api/v1/tracker/taxpayer_status";
		$data = array(
			"inn" => $inn,
			"requestDate" => $dateStr
		);
		$options = array(
			'http' => array(
				'method'  => 'POST',
				'header'  => array(
					'Content-type: application/json',
				),
				'content' => json_encode($data)
			),
		);
	
		$context = stream_context_create($options);
		$result = file_get_contents($url, false, $context);
		$res = json_decode($result, true);

		// {"status":false,"message":"382308035506 не является плательщиком налога на профессиональный доход"}
		

		$db = \Config\Database::connect();
		$save = [
			"inn" => $inn,
			"result" => (int)$res['status'],
			"message" => $res['message'],
			"date_added" => $date->format("Y-m-d H:i:s"),
			"date_updated" => $date->format("Y-m-d H:i:s"),
			"status" => 1,
		];
		$db->table('inn')->insert( $save );
		return $res->false;
	}
    

}