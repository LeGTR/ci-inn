<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>FORM</title>
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    <style>
        .form-text.error{color: red;}
    </style>
</head>
<body>
    <div class="wrapper">
        <div class="container">
            <div class="row">
                <form>
                    <div class="mb-3">
                        <label for="inn" class="form-label">INN</label>
                        <input type="text" class="form-control" name="inn" id="inn" aria-describedby="innHelp">
                        <div id="innHelp" class="form-text">Введите ИНН</div>
                    </div>
                    <div id="result"></div>
                    <button type="submit" class="btn btn-primary post">Submit</button>
                </form>
            </div>
        </div>
    </div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
    $('.post').click(function (e) {
        e.preventDefault();
        $('#innHelp').removeClass('error');
        var inn = $('#inn').val();
        $.post('/post', {inn: inn}, function() {}, "json")
            .done(function(data) {
                console.log( data );

                if ( !data.status ) {
                    $('#result').text('');
                    $('#innHelp').addClass('error');
                    if ( data.massage ) {
                        $('#innHelp').text(data.massage);
                    }else{
                        $('#innHelp').text(data.errors.inn);
                    }
                }else{
                    $('#innHelp').text('Введите ИНН');
                    $('#result').text(data.massage);
                }
            })
            .fail(function() {            
            })
            .always(function() {
            });
    })
</script>
</body>
</html>