<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Inn extends Migration
{
    public function up()
        {
            $this->forge->addField([
                    'inn_id'          => [
                            'type'           => 'INT',
                            'constraint'     => 8,
                            'unsigned'       => true,
                            'auto_increment' => true,
                    ]
            ]);
            $this->forge->addField([
                    'inn'          => [
                            'type'           => 'VARCHAR',
                            'constraint'     => 12,
                    ]
            ]);
            $this->forge->addField([
                    'result'          => [
                            'type'           => 'VARCHAR',
                            'constraint'     => 255,
                    ]
            ]);
            $this->forge->addField([
                    'message'          => [
                            'type'           => 'VARCHAR',
                            'constraint'     => 255,
                    ]
            ]);
            
            $this->forge->addField([
                    'date_added'          => [
                            'type'           => 'DATETIME',
                            //'default'        => 'CURRENT_TIMESTAMP'
                    ]
            ]);
            $this->forge->addField([
                    'date_updated'          => [
                            'type'           => 'DATETIME',
                    ]
            ]);
            $this->forge->addField([
                    'date_deleted'          => [
                            'type'           => 'DATETIME',
                    ]
            ]);
            
            $this->forge->addField([
                    'status'          => [
                            'type'           => 'INT',
                            'constraint'     => 2,
                    ]
            ]);
            $this->forge->addField([
                    'deleted'          => [
                            'type'           => 'INT',
                            'constraint'     => 2,
                    ]
            ]);

            $this->forge->addKey('inn_id', true);
            $this->forge->createTable('inn');
        }

        public function down()
        {
                $this->forge->dropTable('inn');
        }
}
