<?php

namespace App\Controllers;

use App\Models\InnModel;

class Home extends BaseController
{
    protected $request;
    public function __construct() {
		$this->request = \Config\Services::request();
    }
    public function index()
    {
        return view('welcome_message');
    }

    public function form()
    {
        return view('form');
    }

    public function post()
    {
		$_post = $this->request->getPost();
		$validation =  \Config\Services::validation();
        //return $this->response->setJSON($_post);
        
        $validationRules = [
            'inn'            =>[
                'rules'  => 'required|numeric|max_length[12]|min_length[10]|inn1|inn2|innOnline',
                'errors' => [
                    'required' => 'ИНН не введен!',
                    'numeric' => 'Значение должно быть числом',
                    'min_length' => 'Необходимо ввести значение больше 10 символов',
                    'max_length' => 'Необходимо ввести значение меньше или равное 12 символам',
                    'inn1' => 'ИНН не физического лица',
                    'inn2' => 'ИНН не прошел проверку контрольной суммы',
                    'innOnline' => 'ИНН проверка не пройдена',
                ],
            ],
        ];

        $errors = [];
        $validation->setRules($validationRules);
        if (!$validation->run( (array)$_post )) {
            $errors = $validation->getErrors();
        }
        $return = [
            "status" => empty($errors),
            "errors" => $errors
        ];

        $model = new InnModel();
        $_inn = $model->where('inn', $_post['inn'])->get();
        if ( $_inn->getRow() ) {
            $_inn = $_inn->getRow();
            $return["status"] = (bool)$_inn->result;
            $return["massage"] = $_inn->message;
        }
        return $this->response->setJSON($return);
    }
}
