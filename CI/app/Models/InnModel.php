<?php namespace App\Models;
 
use CodeIgniter\Model;
 
class InnModel extends Model{

    protected $table      = 'inn';
    protected $primaryKey = 'inn_id';

    protected $returnType     = 'array';
	protected $useSoftDeletes = false;

    protected $allowedFields = [
        "inn",
        "result",
        "message",
        "status",
        "deleted",
    ];

	protected $useTimestamps = true;
    protected $createdField  = 'date_added';
    protected $updatedField  = 'date_updated';
    protected $deletedField  = 'date_deleted';
	//protected $dateFormat  	 = 'int';

    protected $validationRules    = [
    ];
    protected $validationMessages = [];
    protected $skipValidation     = false;
	// this runs after field validation
	protected $beforeInsert = [];
	protected $beforeUpdate = [];
    protected $beforeDelete = [];


    protected $builder;

    public function __construct(Type $var = null) {
		parent::__construct();
        $this->builder = $this->builder();
    }

    
}